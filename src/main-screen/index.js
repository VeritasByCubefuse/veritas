/*global chrome*/

import React from "react";
import { Alert, Button, Modal, Card, ProgressBar, Container, Row, Col } from 'react-bootstrap';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import axios from 'axios';

//const percentage = 66;
//const axios = require('axios').default;

class MainScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            domain: '',
            tweetId: '',
            class:'',
            content:'',
            author:'',
            accuracy:'',
            headlines: [],
        }
    }

    componentDidMount() {
        // var popupWindow = window.open(
        //     chrome.extension.getURL("index.html"),
        //     "headlineFetcher",
        //     "width=400,height=400"
        // );
        chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
            //const url = new URL(tabs[0].url);
            var url = tabs[0].url;
            const domain = url.hostname;
            var tweetId = url.substring(url.lastIndexOf('/') + 1)
            this.setState({
                // domain: tabs[0].url,//JSON.stringify(tabs[0]),
                tweetId: tweetId,
            });
            this.getAccuracy(domain);
        });
    }


    getAccuracy(query) {
        axios.get('https://veritasbycubefuse-prediction-models.veritas.cubefuse.io/predict',{
          params: {
            tweet_id : this.state.tweetId
          }
        }).then(response  => {
            // this.setState({
            //   tweetId: results
            // });
            var result = response.data;
            console.log(result);
            console.log(result.class);
            this.setState({
                  class: String(result.class).toUpperCase(),
                  content: result.tweet.text,
                  author: result.tweet.user.name,
                });
            if(result.outputs[0]>result[1]){
                this.setState({
                    accuracy: String((result.outputs[0]*(100)).toFixed(2)).toUpperCase()
                  });
            }
            else{
                this.setState({
                    accuracy: String((result.outputs[1]*(100)).toFixed(2)).toUpperCase()
                  });
            }
            console.log(this.state.class);
        }).catch(error => {
            console.log('Error ', error);
            this.setState({
                class: "Oops! Something went wrong. Please try again"
              });
        });
      }

      
    render = () => {
        return (
            <div style={{ margin: '10%' }}>
                <Alert variant="success">
                    <Alert.Heading>{this.state.author}</Alert.Heading>
                    <p>
                        {this.state.content}
                    </p>
                    {/* <p>Twiiter Id: {this.state.tweetId}</p> */}
                </Alert>
                <div style={{ width: 200, height: 200, marginTop: '5%', marginLeft: '33%' }}>
                    <CircularProgressbar
                        value={this.state.accuracy}
                        strokeWidth='15'
                        text={`${this.state.accuracy}%`}
                        styles={buildStyles({
                            // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
                            strokeLinecap: 'round',
                            // Text size
                            textSize: '16px',
                            // How long animation takes to go from one percentage to another, in seconds
                            pathTransitionDuration: 0.5,
                            // Can specify path transition in more detail, or remove it entirely
                            // pathTransition: 'none',
                            // Colors
                            pathColor: `rgba(26, 188, 156, ${this.state.accuracy / 100})`,
                            textColor: '#2C3E50',
                            trailColor: '#EAECEE',
                            backgroundColor: '#3e98c7',
                        })}
                    />

                </div>

                <div>
                    <h2 style={{ textAlign: "center", marginTop: '4%' }}>{this.state.class}</h2>
                    <hr></hr>
                </div>
                <div>
                    <p style={{ float: "left" }}>Veritas by Cubefuse</p>


                    {/* <Button style={{ float: "right" }} variant="outline-danger">Close</Button> */}
                </div>
            </div>

        );
    };
}

export default MainScreen;
