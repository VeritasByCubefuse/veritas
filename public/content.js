chrome.runtime.onMessage.addListener(request => {
  if (request.type === 'getTwitterId') {
    const modal = document.createElement('dialog');
    modal.setAttribute("style", "height:70%; width:40%");
    modal.innerHTML =
      `<iframe id="Veritas" style="height:100%; width:100%"></iframe>
        <div style="position:absolute; top:5%; right:13%;">  
        <button type="button" style="background-color:transparent;
                                     border-radius:5px;
                                     border:1px solid #c9312c;
                                     display:inline-block;
                                     cursor:pointer;
                                     color:#c9312c;
                                     font-family:Arial;
                                     font-size:17px;
                                     padding:14px 16px;
                                     text-decoration:none;">Close</button>
        </div>`;
    document.body.appendChild(modal);
    const dialog = document.querySelector("dialog");
    dialog.showModal();

    const iframe = document.getElementById("Veritas");  
    iframe.src = chrome.extension.getURL("index.html");
    iframe.frameBorder = 0;
    dialog.querySelector("button").addEventListener("click", () => {
        dialog.close();
     });
  }
});