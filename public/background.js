chrome.contextMenus.create({
    id: 'Veritas',
    title: 'Fake News Identification on Twitter',
    //contexts: ['all'] create context menu (RMB)
    contexts: ['all'],
    documentUrlPatterns: [
        '*://twitter.com/*'
    ]
});


chrome.contextMenus.onClicked.addListener(() => {
    chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
        chrome.tabs.sendMessage(tabs[0].id, { type: 'getTwitterId' });
    });

});
